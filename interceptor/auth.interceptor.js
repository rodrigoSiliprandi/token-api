const authHelper = require('../helper/auth.helper')

module.exports = (app) => {

    app.use((req, res, next) => {

        if (req.url != '/user/login') {
            try {
                let token = req.header('Authorization')
                let payload = authHelper.validateToken(token)
                req.userId = payload.sub                

                next()
            } catch (error) {
                res.status(401)
                res.statusMessage ="Usuario nao autorizado"
                res.end()
            }

        } else {
            next()
        }
    })

}