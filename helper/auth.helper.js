const jwt = require('jsonwebtoken')

const privateKey = '%&#5i573m4s=+/'

exports.createToken = (user) => {

    let payload = {
        user: user.name,
        roles: user.roles
    }

    return jwt.sign(payload, privateKey, {
        subject: user._id.toString(), 
        expiresIn: 600
    })
}

exports.validateToken = (token) => {
    let authToken = token.substring('Bearer '.length)
    return jwt.verify(authToken, privateKey)
}